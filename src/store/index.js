
import userInfo from './modules/userInfo'
import session from './modules/session'
import psychologist from './modules/psychologist'

export default {
  modules: {
    userInfo,
    session,
    psychologist
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
}
