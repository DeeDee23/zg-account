import avatar from '@/assets/images/Гаевская.png'

export default {
  state: {
    psychologistName: 'Имя',
    psychologistAvatar: avatar,
    psychologistLink: '1',
    psychologistSkypeId: 'live:fec8026fb3577304'
  },
  mutations: {
    psychologistChange (state, data) {
      state.psychologistName = data.name
      state.psychologistAvatar = data.avatar
      state.psychologistLink = data.link
      state.psychologistSkypeId = data.skypeId
    }
  },
  actions: {
    setPsychologist ({ commit }, data) {
      commit('psychologistChange', data)
    }
  }
}
