export default {
  state: {
    username: 'Станислав',
    id: 1,
    avatar: null,
    email: 'test@test.ru',
    phone: 9876694167,
    hasPromo: true,
    promoPoints: 1000,
    socials: [{
      name: 'google',
      id: ''
    }, {
      name: 'facebook',
      id: ''
    }, {
      name: 'vk',
      id: ''
    }]
  },
  mutations: {
    usernameChange (state, name) {
      state.username = name
    },
    emailChange (state, email) {
      state.email = email
    },
    phoneChange (state, phone) {
      state.phone = phone
    },
    idChange (state, id) {
      state.id = id
    },
    avatarChange (state, avatar) {
      state.avatar = avatar
    },
    socialsChange (state, socials) {
      state.socials = socials
    }
  },
  actions: {
    setUsername ({ commit }, username) {
      commit('usernameChange', username)
    },
    setEmail ({ commit }, email) {
      commit('emailChange', email)
    },
    setPhone ({ commit }, phone) {
      commit('phoneChange', phone)
    },
    setAvatar ({ commit }, avatar) {
      commit('avatarChange', avatar)
    },
    setUserInfo ({ commit }, data) {
      // commit('usernameChange', data.username)
      // commit('idChange', data.id)
      // commit('avatarChange', data.avatar)
      // commit('socialsChange', data.socials)
    },
    addSocial ({ commit, state }, socialData) {
      const current = Object.assign({}, state.socials)
      const social = current.find(item => item.name === socialData.name)

      if (social) {
        social.link = socialData.link
      }

      commit('userSocialsChange', current)
    }
  }
}
