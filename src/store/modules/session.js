export default {
  state: {
    nextSessionDate: new Date(2019, 8, 10, 12, 0, 0),
    hasNextSession: false,
    sessionConfirmedStatus: false,
    sessionsPaidCount: 0
  },
  mutations: {
    nextSessionDateChange (state, date) {
      state.nextSessionDate = date
    },
    sesionConfirmedStatusChange (state, confirmed) {
      state.sessionConfirmedStatus = confirmed
    },
    sessionsPaidCountChange (state, count) {
      state.sessionsPaidCount = count
    },
    hasNextSession (state, hasNextSession) {
      state.hasNextSession = hasNextSession
    }
  },
  actions: {
    setSessionDate ({ commit }, date) {
      commit('nextSessionDateChange', date)
    },
    setSessionConfimedStatus ({ commit }, confirmed) {
      commit('sesionConfirmedStatusChange', confirmed)
    },
    setSessionsPaidCount ({ commit }, count) {
      commit('sessionsPaidCountChange', count)
    },
    setHasNextSession ({ commit }, hasNextSession) {
      commit('hasNextSession', hasNextSession)
    }
  }
}
