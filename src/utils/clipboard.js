const fallbackCopyTextToClipboard = function (text) {
  const textarea = document.createElement('textarea')
  textarea.value = text
  document.body.appendChild(textarea)
  textarea.focus()
  textarea.select()

  try {
    const successful = document.execCommand('copy')
    const msg = successful ? 'successful' : 'unsuccessful'
    console.log('Fallback: Copying text command was ' + msg)
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err)
  }

  document.body.removeChild(textarea)
}

const copyTextToClipboard = function (text) {
  if (!window.navigator) {
    return fallbackCopyTextToClipboard(text)
  }

  navigator.clipboard
    .writeText(text)
    .then(
      () => {
        console.log('Async: Copying to clipboard was successful!')
      },
      (err) => {
        console.error('Async: Could not copy text: ', err)
      })
}

export {
  copyTextToClipboard
}
