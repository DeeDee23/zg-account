import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import StartSession from './views/StartSession'
import Schedule from './views/Schedule'
import Pay from './views/Pay'
import Cancel from './views/Cancel'
import Success from './views/Success'
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  }, {
    path: '/start-session',
    name: 'startSession',
    component: StartSession
  }, {
    path: '/schedule/new-session-date',
    name: 'newSessionDate',
    component: Schedule
  }, {
    path: '/schedule/change-session-date',
    name: 'changeSessionDate',
    component: Schedule
  }, {
    path: '/pay',
    name: 'pay',
    component: Pay
  }, {
    path: '/pay/paid-left',
    name: 'paidLeft',
    component: Success,
    props: true
  }, {
    path: '/cancel',
    name: 'cancel',
    component: Cancel
  }, {
    path: '/success',
    name: 'success',
    component: Success,
    props: true
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
  }
  ]
})
